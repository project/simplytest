module.exports = {
  purge: {
    content: ["templates/**/*.twig", "lib/**/*.js"],
    options: {
      keyframes: true
    }
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "yellow-tan": "#ffde65",
        "sky-blue": "#5ccdff",
        "dark-sky-blue": "#2da8df",
        "flat-blue": "#3877af",
        "light-blue": "#ecf6fd",
        "title-blue": "#16426d",
        "dark-blue": "#18222c"
      }
    }
  },
  variants: {
    extend: {
      cursor: ["disabled"]
    }
  }
};
